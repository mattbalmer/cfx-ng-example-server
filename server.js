var config = require('config'),
    mongoose = require('mongoose-q')(),
    express = require('express');

// === Mongoose Models ===
mongoose.connect(config.mongo_uri);
require('./schemas/Sandwich');

// === Express Server ===
var server = express();

server.use( function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'https://cfx-ng-example-c9-mattbalmer.c9.io');
    next();
});

server.use( require('cors')() );
server.use( require('body-parser')() );

server.use( '/1.0', require('./api-1.0/routes') );

server.listen(config.port, function() {
    console.log('Server listening on port %s', config.port);
});