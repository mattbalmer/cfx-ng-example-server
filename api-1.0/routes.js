var express = require('express'),
    mongoose = require('mongoose-q')();

var routes = module.exports = express.Router();

routes.use( '/sandwiches', require('./controllers/sandwiches').export() );