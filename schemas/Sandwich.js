var mongoose = require('mongoose-q')();

var Schema = new mongoose.Schema({
    title: {
        required: true,
        type: String
    },
    description: String,
    price: Number,
    date_added: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Sandwich', Schema);