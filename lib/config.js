module.exports = {
    local: {
        environment: 'local',
        port: 3010,
        mongo_uri: 'mongodb://localhost/cfx-ng-example'
    },
    production: {
        environment: 'production',
        port: process.env.PORT || 80,
        mongo_uri: 'mongodb://heroku_app31343512:614v41h7g85b3jse9ihfn3ksuv@ds051640.mongolab.com:51640/heroku_app31343512'
    }
}[process.env.NODE_ENV];